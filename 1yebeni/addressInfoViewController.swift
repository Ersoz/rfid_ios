//
//  addressInfoViewController.swift
//  1yebeni
//
//  Created by serdar ersöz on 12/11/19.
//  Copyright © 2019 serdar ersöz. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class addressInfoViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        
        
        
             filteredTableData = selectedData.array!.filter {
                return (($0["name"].string)!.lowercased().contains(searchController.searchBar.text!.lowercased()))
                   }
        self.addressTableView.reloadData()

    }
    
 
    
    @IBOutlet weak var address1: UITextField!
    
    @IBOutlet weak var addressTableView: UITableView!
    
    @IBOutlet weak var cityBtn: UIButton!
    @IBOutlet weak var contryBtn: UIButton!
    @IBOutlet weak var address2: UITextField!
    var countries = JSON()
    var selectedData = JSON()
    let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)

    var tableData = [String]()
    var filteredTableData = [JSON]()
    var resultSearchController = UISearchController()
    
    var dataName = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        resultSearchController = ({
                let controller = UISearchController(searchResultsController: nil)
                controller.searchResultsUpdater = self
                controller.dimsBackgroundDuringPresentation = false
                controller.searchBar.sizeToFit()
        
            

                addressTableView.tableHeaderView = controller.searchBar

                return controller
            })()
        addressTableView.reloadData()
        self.hideKeyboardWhenTappedAround()

        contryBtn.contentHorizontalAlignment = .left

        contryBtn.setTitleColor(.white, for: .normal)
        contryBtn.setTitle("Turkey", for: .normal)
        contryBtn.tag = 134
        contryBtn.titleEdgeInsets = UIEdgeInsets(top: 0,left: 10,bottom: 0,right: 0)
        
        cityBtn.setTitle("Istanbul", for: .normal)
        cityBtn.tag = 2101
        cityBtn.contentHorizontalAlignment = .left
        cityBtn.setTitleColor(.white, for: .normal)
        cityBtn.titleEdgeInsets = UIEdgeInsets(top: 0,left: 10,bottom: 0,right: 0)

        
        address1.setLeftPaddingPoints(10)

        address2.setLeftPaddingPoints(10)


        
     address1.attributedPlaceholder = NSAttributedString(string: "Adress1",
     attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 1)])

        address2.attributedPlaceholder = NSAttributedString(string: "Adress2",
        attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 1)])
        

 
        
        
        // Do any additional setup after loading the view.
    }
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print("count")
        print(filteredTableData)
        if  (resultSearchController.isActive) {
            return filteredTableData.count ?? 0
         } else {
             return self.selectedData.array?.count ?? 0
         }
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {
        
    }
    func textFieldDidBeginEditing(textField: UITextField) {
    print("TextField did begin editing method called")
    }
   @objc func tapped(textField: UITextField) {
    print("TextField ")
    }

  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        print("cellll")
        
        if (resultSearchController.isActive) {
            print("active search")
            cell.textLabel?.text = filteredTableData[indexPath.row]["name"].string

             return cell
         }
         else {
                   cell.textLabel?.text = self.selectedData[indexPath.row]["name"].string
            cell.tag = self.selectedData[indexPath.row]["id"].int!
            

             return cell
         }
        

             return cell
    }
    
    @IBAction func registerAddressBtnHandler(_ sender: Any) {
        print(cityBtn.tag)
        if address1.text == "" || cityBtn.tag == 0 || address2.text == "" || contryBtn.tag  == 0 {
            
            self.alert(message: "Lütfen tüm alanları doldurunuz.")
        }else {
            AddressHandler.RegisterAddress(country_id: contryBtn.tag, city_id: cityBtn.tag, district: address2.text!, address: address1.text!) { (JSON) in
                     
                if let error = JSON["error"].string {
                    
                    self.alert(message: JSON["messages"][0][0].string ?? "")
                    
                }else {
                    
                       let billingAddress = self.address1.text
                    UserDefaults.standard.set(billingAddress, forKey: "billingAddress")
                    UserDefaults.standard.synchronize()

                       NotificationCenter.default.post(name: Notification.Name("addressComplate"), object: nil)

                    self.dismiss(animated: true, completion: nil)
                }
                     
                 }
        }
        
    }
    @IBAction func cityBtnHandler(_ sender: Any) {
        
    self.addressTableView.isHidden = false
        
        self.dataName = "city"

        AddressHandler.getCity(id: String(contryBtn.tag)) { (JSON) in
            
            self.selectedData = JSON
            
            self.tableData.removeAll()
            for var i in JSON.array ?? [] {
                self.tableData.append(i["name"].string!)

            }
            self.addressTableView.reloadData()

        }
    }
    
    @IBAction func contryBtnHandler(_ sender: Any) {
        self.dataName = "country"

        self.addressTableView.isHidden = false
            
         
            AddressHandler.getCountries { (JSON) in
                                        
                                        self.selectedData = JSON
                self.tableData.removeAll()
                         for var i in JSON.array ?? [] {
                             self.tableData.append(i["name"].string!)

                         }
                                        
                          self.addressTableView.reloadData()
                                        
                                    }
           
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
        
        var selectedID = Int()
        var selectedCountry = String()
        print("selected row")
        if (resultSearchController.isActive) {

            selectedID =  filteredTableData[indexPath.row]["id"].int!

            selectedCountry  = filteredTableData[indexPath.row]["name"].string!
        }else {
             selectedCountry = selectedData[indexPath.row]["name"].string ?? ""
            selectedID =  selectedData[indexPath.row]["id"].int!
        }
        
        
        if dataName == "city"{
            self.cityBtn.setTitle(selectedCountry, for: .normal)
                   self.cityBtn.tag = selectedID ?? 0
        }else if dataName == "country"{
            if self.contryBtn.tag != selectedID {
                
                self.cityBtn.setTitle("Select City", for: .normal)
                self.cityBtn.tag = 0
            }
            self.contryBtn.setTitle(selectedCountry, for: .normal)

            self.contryBtn.tag = selectedID ?? 0
        }
       
        
        
        
        self.addressTableView.isHidden = true
        self.resultSearchController.isActive = false
    
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
