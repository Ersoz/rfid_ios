//
//  ReceiptViewController.swift
//  1yebeni
//
//  Created by serdar ersöz on 11/28/19.
//  Copyright © 2019 serdar ersöz. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class ReceiptViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {


    @IBOutlet weak var receiptTableView: UITableView!
    
    @IBOutlet weak var descLabel: UILabel!
    var receiptJSON = JSON()
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        getReceipt()

    }
    func getReceipt ()->Void {
           var url = UrlPool.PAYMENT_HISTORY
               

        Alamofire.request(url, method: .get, parameters:nil, encoding: URLEncoding.default, headers:FirstViewController.headers).validate().responseJSON { response in
                          
                          
                          switch response.result {
                          case .success(let value):
                              let json = JSON(value)
                              
                              print("get cards")
                              print(json[9])
            
                              self.receiptJSON = json
                              self.receiptTableView.reloadData()
                              
                              if self.receiptJSON.array?.count == 0 {
                                self.receiptTableView.isHidden = true
                                self.descLabel.isHidden = false
                              }else {
                                self.receiptTableView.isHidden = false
                                self.descLabel.isHidden = true
                            }

                          case .failure(let error):
                            print("get reciept error: \(response.debugDescription)")
                            
                          }
                          
                          
                          
                          
                                            
                      
                  
              }

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return receiptJSON.array?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  cell = receiptTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ReceiptTableViewCell
                   
        print(receiptJSON[indexPath.row]["sold_items"][0]["product_name"].string)
        let soldProducts =  receiptJSON[indexPath.row]["sold_items"].array ?? []
        var price = 0.0
        var price_currency = "tl"
        var productName = ""

        for  i in soldProducts {
            productName =  productName +  ( i["product_name"].string ?? "") + " x " + (String(i["count"].int ??  0 ))
            cell.productLabel.text = productName
            price =  Double(i["price"].int ?? 0) + price

            price_currency = i["currency_unit"].string ?? "tl"
           }
        cell.dateLabel.text = receiptJSON[indexPath.row]["created_at"].string
        cell.totalPriceLabel.text = String(price) + " " + price_currency

        
        let status = receiptJSON[indexPath.row]["status_id"].int
        if status == 4 {
            cell.statusLabel.text = "Ödendi"
            cell.statusLabel.textColor = UIColor(red: 25/255, green: 79/255, blue: 65/255, alpha: 1.0)
        }
               
   

           return cell
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
