//
//  FirstViewController.swift
//  1yebeni
//
//  Created by serdar ersöz on 11/27/19.
//  Copyright © 2019 serdar ersöz. All rights reserved.
//

import UIKit
import AVFoundation
import QRCodeReader
import Alamofire
import SwiftyJSON
import  FirebaseMessaging
import CoreLocation

class FirstViewController: UIViewController,QRCodeReaderViewControllerDelegate,CLLocationManagerDelegate {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var barcodeBtn: UIButton!
    @IBOutlet weak var descLabel: UILabel!
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
            
            // Configure the view controller (optional)
            $0.showTorchButton        = false
            $0.showSwitchCameraButton = false
            $0.showCancelButton       = true
            $0.showOverlayView        = true
            $0.rectOfInterest         = CGRect(x: 0.2, y: 0.2, width: 0.6, height: 0.6)
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    @IBOutlet weak var retryPaymenView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    public static var  headers = HTTPHeaders ()

    override func viewDidLoad() {
        super.viewDidLoad()

      
    
               
        barcodeBtn.contentHorizontalAlignment = .fill
        barcodeBtn.contentVerticalAlignment = .fill
        barcodeBtn.imageView?.contentMode = .scaleAspectFill
        
        if let  accessToken =  UserDefaults.standard.string(forKey: "accessToken"){

            
            print(accessToken)
        FirstViewController.headers = [
                                                 "Accept": "application/json",
                                                 "Authorization":"Bearer " + accessToken,
                                                 "language-id" :  Locale.current.languageCode ?? "tr" ,
                                                 "os":"ios",
                                                "app_version" : Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String ?? ""

                                             ]
            
            let backgroundImage = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height*0.3))
                backgroundImage.contentMode = UIView.ContentMode.scaleAspectFill
                backgroundImage.image = UIImage(named: "bg_header")
                         self.view.insertSubview(backgroundImage, at: 0)
            
            self.transNavigationBar()
            
            
            UserInfoHandler.getUserInfo { (JSON) in
                print("user api")
                print((JSON))
                
                self.nameLabel.text = "Hello " + (JSON["name"].string ?? "")
                
                UserDefaults.standard.set(JSON["name"].string ?? "", forKey: "userName")
                UserDefaults.standard.set(JSON["id"].int ?? "", forKey: "userID")
                UserDefaults.standard.set(JSON["email"].int ?? "", forKey: "email")


                UserDefaults.standard.synchronize()
            
            }
            
            

            //firebase tokenı alır
                if let token = Messaging.messaging().fcmToken {
                 print("fb token")
                    print(token)
                 self.updatePush(token: token)
                }
        }
      
        
        // Do any additional setup after loading the view.
    }
    func updatePush (token:String) -> Void {
         var url = UrlPool.USER_PUSH
                   

        Alamofire.request(url, method: .post, parameters:["token":token], encoding: URLEncoding.default, headers:FirstViewController.headers).validate().responseJSON { response in
                              
                              
                              switch response.result {
                              case .success(let value):
                                print("push")
                                  let json = JSON(value)
                                  print(json)
                               

                              case .failure(let error):
                                print("push  error: \(response.debugDescription)")
                                
                              }
                              
                              
                              
                              
                                                
                          
                      
                  }
            

        }
        
        
        static func RegisterAddress ( country_id:Int,city_id:Int,district:String,address:String, completion: ((JSON) -> Void)?)  {
            
                      

            var url = UrlPool.REGISTER_ADDRESS
                   
            var responseJSON = JSON()

            Alamofire.request(url, method: .post, parameters:["country_id":country_id,"city_id":city_id,"district":district,"address":address], encoding: URLEncoding.default, headers:FirstViewController.headers).validate().responseJSON { response in
                              
                              
                              switch response.result {
                              case .success(let value):
                                  let json = JSON(value)
                                  
                                  print("get countries")
                                  print(json)
                
                                  completion!(json)

                              case .failure(let error):
                                print("get cards error: \(response.debugDescription)")
                                if let data = response.data {
                                
                            
                                    let json = String(data: data, encoding: String.Encoding.utf8)
                                    
                                    let jsonError = JSON(data)
                                                                                                
                                    print("Failure Response: \(jsonError)")

                                    completion!(jsonError)
                                                                                             
                                    }
                                
                              }
                              
                              
                              
                              
                                                
                          
                      
                  }
            
    }
    @IBAction func paymentMethodsBtnHandler(_ sender: Any) {
        
     
        self.tabBarController?.selectedIndex = 3

    }
    @IBAction func retryPaymenHandler(_ sender: Any) {
        self.view.activityStartAnimating(activityColor: UIColor.black, backgroundColor: UIColor.black.withAlphaComponent(0.3))
                       
        PaymentHandler.retryPayment { (JSON) in
            
            self.view.activityStopAnimating()
            print(JSON)
           
            if let error = JSON["error"].string {
                
                if let message = JSON["message"].string {
                               
                               self.alert(message: message)
                           }
            }else {
                self.retryPaymenView.isHidden = true
            }
        }
       
    }
    override func viewWillAppear(_ animated: Bool) {

        
        PaymentHandler.checkPayment { (JSON) in
            
            print("check payment \(JSON)")
            if let hasFailPayment = JSON["has_failed_payment"].bool {
                
                if hasFailPayment {
                    self.retryPaymenView.isHidden = false
                }else {
                    self.retryPaymenView.isHidden = true

                }
            }
        }

    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent //.default for black style
    }
    
    @IBAction func barcodeBtnHandler(_ sender: Any) {
        
        
                
        
         // Retrieve the QRCode content
         // By using the delegate pattern
        if let  hasCreditCard =  UserDefaults.standard.value(forKey: "hasCreditCard") as? Bool{
            if hasCreditCard {
                
                self.readerVC.delegate = self

                                  // Or by using the closure pattern
                                 self.readerVC.completionBlock = { (result: QRCodeReaderResult?) in
                                     
                                     print("qr r")
                                     
                                    
                                     if let code = result?.value {
                                         
                                         print(code)
                                         self.openDoor(code: result.unsafelyUnwrapped.value)
                                     }
                                  }

                                  // Presents the readerVC as modal form sheet
                                 self.readerVC.modalPresentationStyle = .formSheet
                                 
                                 self.present(self.readerVC, animated: true, completion: nil)
                
            }else {
                self.view.activityStartAnimating(activityColor: UIColor.black, backgroundColor: UIColor.black.withAlphaComponent(0.3))
                
                PaymentHandler.getCards { (JSON) in
                         
                    self.view.activityStopAnimating()
                         if JSON.array?.count ?? 0 > 0 {

                             UserDefaults.standard.set(true, forKey: "hasCreditCard")
                                                                UserDefaults.standard.synchronize()
                             self.readerVC.delegate = self

                              // Or by using the closure pattern
                             self.readerVC.completionBlock = { (result: QRCodeReaderResult?) in
                                 
                                 print("qr r")
                                 
                                
                                 if let code = result?.value {
                                     
                                     print(code)
                                     self.openDoor(code: result.unsafelyUnwrapped.value)
                                 }
                              }

                              // Presents the readerVC as modal form sheet
                             self.readerVC.modalPresentationStyle = .formSheet
                             
                             self.present(self.readerVC, animated: true, completion: nil)

                         }else {
                             
                             
                             UserDefaults.standard.set(false, forKey: "hasCreditCard")
                                                                UserDefaults.standard.synchronize()
                             
                             DispatchQueue.main.async(execute: {

                               let alert = UIAlertController(title:  "Uyarı" , message: "Alışveriş yapmak için kredi kartını eklemelisin." , preferredStyle: .alert)
                             alert.addAction(UIAlertAction(title: "Okey", style: .default, handler: { (UIAlertAction) in
                                 var sb = UIStoryboard(name: "Main", bundle: nil)
                                                    
                                                    var popup = sb.instantiateViewController(withIdentifier: "addCardView") as! PaymentMethodViewController
                                                    self.present(popup, animated: true, completion: nil)
                             }))

                            self.present(alert, animated: true, completion: nil)
                             
                             })
                             
                         }
                         
                     }
            }
        }else {

            self.view.activityStartAnimating(activityColor: UIColor.black, backgroundColor: UIColor.black.withAlphaComponent(0.3))
            
            PaymentHandler.getCards { (JSON) in
                
                self.view.activityStopAnimating()
                if JSON.array?.count ?? 0 > 0 {

                    UserDefaults.standard.set(true, forKey: "hasCreditCard")
                                                       UserDefaults.standard.synchronize()
                    self.readerVC.delegate = self

                     // Or by using the closure pattern
                    self.readerVC.completionBlock = { (result: QRCodeReaderResult?) in
                        
                        print("qr r")
                        
                       
                        if let code = result?.value {
                            
                            print(code)
                            self.openDoor(code: result.unsafelyUnwrapped.value)
                        }
                     }

                     // Presents the readerVC as modal form sheet
                    self.readerVC.modalPresentationStyle = .formSheet
                    
                    self.present(self.readerVC, animated: true, completion: nil)

                }else {
                    
                    
                    UserDefaults.standard.set(false, forKey: "hasCreditCard")
                                                       UserDefaults.standard.synchronize()
                    
                    DispatchQueue.main.async(execute: {

                      let alert = UIAlertController(title:  "Uyarı" , message: "Alışveriş yapmak için kredi kartını eklemelisin." , preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Okey", style: .default, handler: { (UIAlertAction) in
                        var sb = UIStoryboard(name: "Main", bundle: nil)
                                           
                                           var popup = sb.instantiateViewController(withIdentifier: "addCardView") as! PaymentMethodViewController
                                           self.present(popup, animated: true, completion: nil)
                    }))

                   self.present(alert, animated: true, completion: nil)
                    
                    })
                    
                }
                
            }
        }


             
    }
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        
        reader.stopScanning()

        dismiss(animated: true, completion: nil)
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()

        dismiss(animated: true, completion: nil)
    }
    
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
    }


    func openDoor ( code : String) -> Void {
        print(code)
        Alamofire.request(UrlPool.FRIDGE_OPEN_DOOR, method: .post, parameters:["qrcode":code], encoding: URLEncoding.default, headers:FirstViewController.headers).validate().responseJSON { response in
                          
                          
                          switch response.result {
                          case .success(let value):
                              let json = JSON(value)
                              
                              print("open door")
                              print(json)
            
                        
                              

                          case .failure(let error):
                           print("open door error: \(error.localizedDescription)")
                           print(response.debugDescription)
                            if let data = response.data {
                                                                  let json = String(data: data, encoding: String.Encoding.utf8)
                                                                  let jsonError = JSON(data)
                                                                  
                                                                  print("Failure Response: \(jsonError)")
                                                               
                                
                                   if let message = jsonError["message"].string {
                                
                                self.alert(message: message)
                                if message == "Unauthenticated." {
                                                            
                        // bilgileri siler ve logine atar
                            let domain = Bundle.main.bundleIdentifier!
                            UserDefaults.standard.removePersistentDomain(forName: domain)
                            UserDefaults.standard.synchronize()
                                    print("cache deleted")

                                    
                                                                      
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)

                                    let initialViewController = storyboard.instantiateViewController(withIdentifier: "AuthView")
                                                                      
                        UIApplication.shared.keyWindow?.rootViewController = initialViewController
                                                                    
                                                                  }
                                                                  
                                                                  
                                                              }
                                                         
                                                              }
                            
                          }
                          
                          
                          
                          
                                            
                      
                  
              }
        
    }

}

extension UIViewController {
    func transNavigationBar() -> Void {
        
        
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
       
        
    }
 
    
   
}





