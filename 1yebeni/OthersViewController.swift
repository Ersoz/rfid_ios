//
//  OthersViewController.swift
//  1yebeni
//
//  Created by serdar ersöz on 12/2/19.
//  Copyright © 2019 serdar ersöz. All rights reserved.
//

import UIKit
import MessageUI

class OthersViewController: UIViewController,MFMailComposeViewControllerDelegate {

    @IBOutlet weak var paymentView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let gesture = UITapGestureRecognizer(target: self, action: "payments:")
          // or for swift 2 +
          let gestureSwift2AndHigher = UITapGestureRecognizer(target: self, action:  #selector (self.paymentAction (_:)))
          self.paymentView.addGestureRecognizer(gesture)
        
        // Do any additional setup after loading the view.
    }
    @IBAction func logoutBtnHandler(_ sender: Any) {
        
                           
        // bilgileri siler ve logine atar
            let domain = Bundle.main.bundleIdentifier!
            UserDefaults.standard.removePersistentDomain(forName: domain)
            UserDefaults.standard.synchronize()
                    print("cache deleted")

                    
                                                      
                let storyboard = UIStoryboard(name: "Main", bundle: nil)

                    let initialViewController = storyboard.instantiateViewController(withIdentifier: "AuthView")
                                                      
        UIApplication.shared.keyWindow?.rootViewController = initialViewController
    }
    
    
    @IBAction func contactBtnHandler(_ sender: Any) {
        
        print("contact btn handler")
         let userName = UserDefaults.standard.string(forKey: "userName") ?? ""
        let userID = UserDefaults.standard.integer(forKey: "userID") ?? 0

        if MFMailComposeViewController.canSendMail() {
                          
                          let mail = MFMailComposeViewController()
                          mail.mailComposeDelegate = self
                          mail.setToRecipients(["info@1yebeni.com"])
                        mail.setSubject("Support-\(userName) -\(String(userID))")
                          present(mail, animated: true, completion: nil)
                      } else {
                          print("Cannot send mail")
                          // give feedback to the user
                      }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
          self.dismiss(animated: true)
      }
    @objc func paymentAction(_ sender:UITapGestureRecognizer){


        
         // do other task
      }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
