//
//  CategoryViewController.swift
//  1yebeni
//
//  Created by serdar ersöz on 12/5/19.
//  Copyright © 2019 serdar ersöz. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import Kingfisher

class CategoryViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {


    @IBOutlet weak var categoryCollectView: UICollectionView!
    

    var menuList = JSON()
    var fridgeID = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        FridgeHandler.getMenu(fridgeID: fridgeID, lat: nil, long: nil) { (JSON) in
            
            print("fridge menü \(JSON)")
            
            self.menuList = JSON["menu"]
            
            self.categoryCollectView.reloadData()
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
                let cell = categoryCollectView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategoryCollectionViewCell
        
        

        
        var imgURL =  "http://rfid.jazzcript.com/storage/"  + (self.menuList[0]["image"].string ?? "")

        print("imagg")
        print(self.menuList[indexPath.row]["menu"])
        
    //    cell.image.kf.indicatorType = .activity
                
        cell.name.text =  self.menuList[0]["title"].string
        cell.desc.text =  self.menuList[0]["description"].string
        cell.image.kf.setImage(with: URL(string: imgURL))
               
                
                return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
             var sb = UIStoryboard(name: "Main", bundle: nil)
             
             var popup = sb.instantiateViewController(withIdentifier: "ProductView") as! ProductsViewController
             
        popup.productList = menuList[indexPath.row]["products"]
             self.present(popup, animated: true, completion: nil)
         
        
        print(menuList[indexPath.row]["products"])
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CategoryViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: UIScreen.main.bounds.width/2 - 10, height: UIScreen.main.bounds.width/2)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 3.0
    }
    
   
}
