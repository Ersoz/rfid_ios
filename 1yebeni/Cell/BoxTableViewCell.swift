//
//  BoxTableViewCell.swift
//  1yebeni
//
//  Created by serdar ersöz on 11/28/19.
//  Copyright © 2019 serdar ersöz. All rights reserved.
//

import UIKit

class BoxTableViewCell: UITableViewCell {

    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    var lat = 0.0
    var long = 0.0

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
