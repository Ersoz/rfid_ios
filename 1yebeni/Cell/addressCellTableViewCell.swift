//
//  addressCellTableViewCell.swift
//  1yebeni
//
//  Created by serdar ersöz on 12/13/19.
//  Copyright © 2019 serdar ersöz. All rights reserved.
//

import UIKit

class addressCellTableViewCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var addressDesc: UILabel!
    @IBOutlet weak var addressTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
