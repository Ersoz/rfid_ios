//
//  CategoryCollectionViewCell.swift
//  1yebeni
//
//  Created by serdar ersöz on 12/5/19.
//  Copyright © 2019 serdar ersöz. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var desc: UILabel!
}
