//
//  paymentMethodTableViewCell.swift
//  1yebeni
//
//  Created by serdar ersöz on 12/2/19.
//  Copyright © 2019 serdar ersöz. All rights reserved.
//

import UIKit

class paymentMethodTableViewCell: UITableViewCell {

    @IBOutlet weak var cardImageg: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var cardNumber: UILabel!
    @IBOutlet weak var cardName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
