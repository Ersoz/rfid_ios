//
//  ProductTableViewCell.swift
//  1yebeni
//
//  Created by serdar ersöz on 12/5/19.
//  Copyright © 2019 serdar ersöz. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var name: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
