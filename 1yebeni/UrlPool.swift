//
//  UrlPool.swift
//  1yebeni
//
//  Created by serdar ersöz on 11/30/19.
//  Copyright © 2019 serdar ersöz. All rights reserved.
//

import Foundation

class UrlPool: NSObject {

   // public static let MAIN : String = "http://192.168.1.4:8000/";
 
   public static let MAIN : String = "http://1yebeni.com/";
   // public static let MAIN : String = "http://rfid.jazzcript.com/";


    public static let LOGIN : String =   MAIN + "oauth/token";
    
    public static let FIREBASE_LOGIN : String =   MAIN + "api/user/login/firebase";

    
    public static let REGISTER : String =   MAIN + "api/user/register/user";

    

    public static let ADD_PAYMENT : String =   MAIN + "api/user/register/card";
    
    public static let GET_CARDS : String =   MAIN + "api/user/payment/cards";
    public static let CHECK_PAYMENT : String =   MAIN + "api/user/payment/status";

    

    public static let FRIDGE_OPEN_DOOR : String =   MAIN + "api/user/fridge/open";

    

    public static let FRIDGE_LIST : String =   MAIN + "api/user/fridge/list";

    public static let PAYMENT_HISTORY : String =   MAIN + "api/user/payment/history";
    public static let RETRY_PAYMENT : String =   MAIN + "api/user/payment/retry";


    public static let REGISTER_CARD : String =   MAIN + "api/user/register/card";
    public static let GET_COUNTRIES : String =   MAIN + "api/common/countries";
    public static let GET_CITY: String =   MAIN + "api/common/cities?country_id=";


    
   public static let ADD_CARD : String =   MAIN + "api/user/payment/cards";

    public static let USER_INFO : String =   MAIN + "api/user/info";

   public static let FRIDGE_MENU : String =   MAIN + "api/user/fridge/menu";
    public static let REGISTER_ADDRESS : String =   MAIN + "api/user/address";

    public static let USER_PUSH : String =   MAIN + "api/user/push";



    


}
