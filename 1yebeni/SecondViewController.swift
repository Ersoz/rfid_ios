//
//  SecondViewController.swift
//  1yebeni
//
//  Created by serdar ersöz on 11/27/19.
//  Copyright © 2019 serdar ersöz. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import SwiftyJSON
import Alamofire

class SecondViewController: UIViewController,CLLocationManagerDelegate,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var detailView: UIView!
    
    @IBOutlet weak var detailFridgeName: UILabel!
    
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var addressDetail: UILabel!
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var boxTableView: UITableView!
    var locManager = CLLocationManager()
    var fridgeList = JSON()
    
    public  var myLat : Double =  41.0441875
    public  var myLong : Double =  29.0070588
    var selectedFridgeID = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
          locManager.delegate = self


    // For use in foreground
    self.locManager.requestWhenInUseAuthorization()

    if CLLocationManager.locationServicesEnabled() {
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        locManager.startUpdatingLocation()
    }

    map.isZoomEnabled = true
    map.isScrollEnabled = true

    if let coor = map.userLocation.location?.coordinate{
        map.setCenter(coor, animated: true)
    }
        
        
    }
    @IBAction func showMenuBtnHandler(_ sender: Any) {
        
        
        
        
        var sb = UIStoryboard(name: "Main", bundle: nil)
        
        var popup = sb.instantiateViewController(withIdentifier: "CategoryView") as! CategoryViewController
        popup.fridgeID = self.selectedFridgeID
        self.present(popup, animated: true, completion: nil)
    }
    
    func getFridgeList (lat:String,long:String) -> Void {
        
        
            var url = UrlPool.FRIDGE_LIST
               

        Alamofire.request(url, method: .get, parameters:["lat":lat,"lng":long], encoding: URLEncoding.default, headers:FirstViewController.headers).validate().responseJSON { response in
                          
                          
                          switch response.result {
                          case .success(let value):
                              let json = JSON(value)
                              
                              print("get cards")
                              print(json)
            
                              self.fridgeList = json
                              self.boxTableView.reloadData()
                              if self.fridgeList["fridge_list"].array?.count ?? 0 > 0 {
                                
                                
                                                            self.boxTableView.isHidden = false
                                                            self.descLabel.isHidden = true
                                self.detailView.isHidden = true
                            
                                
                              
                              }else {
                                
                                                              self.boxTableView.isHidden = true
                                                              self.descLabel.isHidden = false
                            
                            }

                          case .failure(let error):
                            print("get fridge list error: \(response.debugDescription)")
                            

                            if let data = response.data {
                                                                  let json = String(data: data, encoding: String.Encoding.utf8)
                                                                  let jsonError = JSON(data)
                                                                  
                                                                  print("Failure Response: \(jsonError)")
                                
                                if let message = jsonError["message"].string {
  
                                    self.alert(message: message)
                                    if message == "Unauthenticated." {
                                        
                                        // bilgileri siler ve logine atar
                                        let domain = Bundle.main.bundleIdentifier!
                                                          UserDefaults.standard.removePersistentDomain(forName: domain)
                                                          UserDefaults.standard.synchronize()
                                        

                                        let initialViewController = self.storyboard?.instantiateViewController(withIdentifier: "AuthView")
                                        
                                        UIApplication.shared.keyWindow?.rootViewController = initialViewController
                                    }
                                    
                                    
                                }
                           
                                                              }
                            
                          }
                          
                          
                          
                          
                                            
                      
                  
              }

        
    }
    
    func showLocation(lat:Double,long:Double) -> Void {
        
        myLat = lat
        myLong = long
        let locValue:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: CLLocationDegrees(floatLiteral: lat), longitude: CLLocationDegrees(floatLiteral: long))
              map.mapType = MKMapType.standard

            let span = MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
              let region = MKCoordinateRegion(center: locValue, span: span)
              map.setRegion(region, animated: true)

              let annotation = MKPointAnnotation()
              annotation.coordinate = locValue
              annotation.title = "Fridge"
        map.showsUserLocation = false

              map.addAnnotation(annotation)
           
     }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return fridgeList["fridge_list"].array?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let  cell = boxTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! BoxTableViewCell
                 
        cell.nameLabel.text = fridgeList["fridge_list"][indexPath.row]["name"].string
        cell.addressLabel.text =  fridgeList["fridge_list"][indexPath.row]["address"].string
        
        cell.lat = fridgeList["fridge_list"][indexPath.row]["latitude"].double!
        cell.long = fridgeList["fridge_list"][indexPath.row]["longitude"].double!

        return cell
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
          let locValue:CLLocationCoordinate2D = manager.location!.coordinate

          map.mapType = MKMapType.standard

        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
          let region = MKCoordinateRegion(center: locValue, span: span)
          map.setRegion(region, animated: true)

        map.showsUserLocation = true
        getFridgeList(lat: String(locValue.latitude), long: String(locValue.longitude))

          locManager.stopUpdatingLocation()
      }


    @IBAction func showMapBtnHandler(_ sender: Any) {
        
        let query = "?daddr=\(myLat),\(myLong)"
          let path = "http://maps.apple.com/" + query
          if let url = NSURL(string: path) {
            UIApplication.shared.openURL(url as URL)
          }
    }
    @IBAction func exitDetailBtnHandler(_ sender: Any) {
        
        self.detailView.isHidden = true
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            let cell = boxTableView.cellForRow(at: indexPath) as! BoxTableViewCell
            
     
            self.detailView.isHidden = false
            showLocation(lat: cell.lat, long: cell.long)

            self.detailFridgeName.text = cell.nameLabel.text
            self.addressDetail.text = cell.addressLabel.text
        
        if let id =  fridgeList["fridge_list"][indexPath.row]["id"].int {
            
            self.selectedFridgeID = String(id)
        }
      
            print(cell.lat)
            
    //
    //
    //
    //        var instagramHooks = "instagram://user?username=" + cell.name.text!
    //        var instagramUrl = NSURL(string: instagramHooks)
    //        if UIApplication.shared.canOpenURL(instagramUrl! as URL) {
    //            UIApplication.shared.openURL(instagramUrl! as URL)
    //        } else {
    //            //redirect to safari because the user doesn't have Instagram
    //            UIApplication.shared.openURL(NSURL(string: "http://instagram.com/")! as URL)
    //        }
        }


}

