//
//  ProductsViewController.swift
//  1yebeni
//
//  Created by serdar ersöz on 12/5/19.
//  Copyright © 2019 serdar ersöz. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class ProductsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var productTableView: UITableView!
    var productList = JSON()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        productList.array?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let  cell = productTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProductTableViewCell
                       
        
        cell.name.text = productList[indexPath.row]["title"].string
              return cell
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
