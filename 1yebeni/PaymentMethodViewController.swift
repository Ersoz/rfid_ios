//
//  PaymentMethodViewController.swift
//  1yebeni
//
//  Created by serdar ersöz on 12/1/19.
//  Copyright © 2019 serdar ersöz. All rights reserved.
//

import UIKit
import Caishen

class PaymentMethodViewController: UIViewController,CardTextFieldDelegate {

    @IBOutlet weak var cardLabel: CardTextField!
    

    @IBOutlet weak var BillingBtn: UIButton!
    @IBOutlet weak var dateLabel: UITextField!
    @IBOutlet weak var surnameLabel: UITextField!
    @IBOutlet weak var creditCardForm: UIStackView!
    @IBOutlet weak var nameLabel: UITextField!
    
    var cardNumber = String ()
    var cvc = Int ()
    var expiryYear = Int ()
    var expiryMonth = Int ()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      NotificationCenter.default.addObserver(
                    self,
                    selector: #selector(addressRefresh),
                    name:NSNotification.Name( "addressComplate"),
                    object: nil)
        self.hideKeyboardWhenTappedAround()

        BillingBtn.contentHorizontalAlignment = .left

             BillingBtn.titleEdgeInsets = UIEdgeInsets(top: 0,left: 10,bottom: 0,right: 0)
        
   

         //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
         //tap.cancelsTouchesInView = false

        
   cardLabel?.cardTextFieldDelegate = self

    
        // Do any additional setup after loading the view.
    }
    
    @objc  func addressRefresh()->Void{
        
        print("refresh")
        if let address = UserDefaults.standard.value(forKey: "billingAddress") as? String {
                  
                  self.BillingBtn.tag = 1
                              self.BillingBtn.setTitleColor(.black, for: .normal)
                              self.BillingBtn.setTitle(address, for: .normal)
            
              }else {
                  self.BillingBtn.tag = 0
                           self.BillingBtn.setTitleColor(.darkGray, for: .normal)

                           self.BillingBtn.setTitle("Billing Address", for: .normal)
              }
               
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("willappear")

         
        if let address = UserDefaults.standard.value(forKey: "billingAddress") as? String {
            
                      self.BillingBtn.tag = 1
                        self.BillingBtn.setTitleColor(.black, for: .normal)
                        self.BillingBtn.setTitle(address, for: .normal)
        }else {
            self.BillingBtn.tag = 0
                     self.BillingBtn.setTitleColor(.darkGray, for: .normal)

                     self.BillingBtn.setTitle("Billing Address", for: .normal)
        }
         
        

    }
  
    func cardTextField(_ cardTextField: CardTextField, didEnterCardInformation information: Card, withValidationResult validationResult: CardValidationResult) {
        
        print("result \(cardTextField.card.bankCardNumber)")
        self.cardNumber = cardTextField.card.bankCardNumber.rawValue
        self.cvc = cardTextField.card.cardVerificationCode.toInt() ?? 0
        self.expiryYear = Int(cardTextField.card.expiryDate.year)
                self.expiryMonth =  Int(cardTextField.card.expiryDate.month)
                
        if validationResult == CardValidationResult.Valid {
            
            print(validationResult.rawValue)
            print(cardTextField.card)
            print(cardTextField.card.bankCardNumber)
        
         
        }
    }
    
    func cardTextFieldShouldShowAccessoryImage(_ cardTextField: CardTextField) -> UIImage? {
        
        print(cardTextField.text)
        
        return nil
        
    
    }
    
    @IBAction func backBtnHandler(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func cardTextFieldShouldProvideAccessoryAction(_ cardTextField: CardTextField) -> (() -> ())? {
        
        print("ghgf")
        
        return nil
    }

    @IBAction func cartBtnHandler(_ sender: Any) {
    
        print(BillingBtn.tag)
        print("card number \(self.cardNumber)")
        if  self.nameLabel.text == "" || BillingBtn.tag == 0 {
            
        
            self.alert(message: "Lütfen tüm alanları doldurunuz.")
        }else {
            PaymentHandler.registerCards(holderName: nameLabel.text!, cardNumber: self.cardNumber, expiryMont: self.expiryMonth, expiryYear: self.expiryYear) { (JSON) in
                  
                if let err = JSON["fail"].string {
                    
                    self.alert(message: "Lütfen bilgilerinizi kontrol ediniz.")
                }else {
                    print("add card")
                                   self.dismiss(animated: true, completion: nil)
                }
               
                  
              }
        }
  
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
