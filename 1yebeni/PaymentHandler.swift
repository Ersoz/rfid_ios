//
//  PaymentHandler.swift
//  1yebeni
//
//  Created by serdar ersöz on 11/30/19.
//  Copyright © 2019 serdar ersöz. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class PaymentHandler: NSObject {
    
    
    static func getCards ( completion: ((JSON) -> Void)?) {
        
                  

            var url = UrlPool.GET_CARDS
               
        var responseJSON = JSON()

        Alamofire.request(url, method: .get, parameters:nil, encoding: URLEncoding.default, headers:FirstViewController.headers).validate().responseJSON { response in
                          
                          
                          switch response.result {
                          case .success(let value):
                              let json = JSON(value)
                              
                              print("get cards")
                              print(json)
            if json.array?.count ?? 0 > 0 {

                                  UserDefaults.standard.set(true, forKey: "hasCreditCard")
                                                                     UserDefaults.standard.synchronize()
                
            }else {
                UserDefaults.standard.set(false, forKey: "hasCreditCard")
                                                                                   UserDefaults.standard.synchronize()
                              
                              }
                              
                              completion!(json)

                          case .failure(let error):
                            print("get cards error: \(response.debugDescription)")
                            
                          }
                          
                          
                          
                          
                                            
                      
                  
              }
        

    }
    
    static func checkPayment ( completion: ((JSON) -> Void)?) {
        
                  

            var url = UrlPool.CHECK_PAYMENT
               
        var responseJSON = JSON()

        Alamofire.request(url, method: .get, parameters:nil, encoding: URLEncoding.default, headers:FirstViewController.headers).validate().responseJSON { response in
                          
                          
                          switch response.result {
                          case .success(let value):
                              let json = JSON(value)
                              
                              print("check payment")
                              print(json)
            
                              completion!(json)

                          case .failure(let error):
                            print("check payment error: \(response.debugDescription)")
                            
                          }
                          
                          
                          
                          
                                            
                      
                  
              }
        

    }
    static func retryPayment ( completion: ((JSON) -> Void)?) {
        
                  

            var url = UrlPool.RETRY_PAYMENT
               
        var responseJSON = JSON()

        Alamofire.request(url, method: .post, parameters:nil, encoding: URLEncoding.default, headers:FirstViewController.headers).validate().responseJSON { response in
                          
                          
                          switch response.result {
                          case .success(let value):
                              let json = JSON(value)
                              
                              print("retry payment")
                              print(json)
            
                              completion!(json)

                          case .failure(let error):
                            print("retry payment error: \(response.debugDescription)")
                            
                          }
                          
                          
                          
                          
                                            
                      
                  
              }
        

    }

    static func deleteCard ( cardID :Int,  completion: ((JSON) -> Void)?)  {
    
                  

            var url = "http://34.65.222.119/api/user/payment/cards/" + String(cardID) 
        
               
        var responseJSON = JSON()

        Alamofire.request(url, method: .delete, parameters:nil, encoding: URLEncoding.default, headers:FirstViewController.headers).validate().responseJSON { response in
                          
                          
                          switch response.result {
                          case .success(let value):
                              let json = JSON(value)
                              
                              print("default cards")
                              print(json)
            
                              completion!(json)

                          case .failure(let error):
                           print("default card error: \(error.localizedDescription)")
                            
                            
                          }
                          
                          
                          
                          
                                            
                      
                  
              }
        

    

    
    
    }
    
    static func setDefault ( cardID :Int,  completion: ((JSON) -> Void)?)  {
    
                  

            var url = "http://34.65.222.119/api/user/payment/cards/" + String(cardID) + "/default"
        
               
        var responseJSON = JSON()

        Alamofire.request(url, method: .put, parameters:nil, encoding: URLEncoding.default, headers:FirstViewController.headers).validate().responseJSON { response in
                          
                          
                          switch response.result {
                          case .success(let value):
                              let json = JSON(value)
                              
                              print("default cards")
                              print(json)
            
                              completion!(json)

                          case .failure(let error):
                           print("default card error: \(error.localizedDescription)")
                            if let data = response.data {
                                                let json = String(data: data, encoding: String.Encoding.utf8)
                                                let jsonError = JSON(data)
                                                
                                                completion!(jsonError)
                                            }

                            
                          }
                          
                          
                          
                          
                                            
                      
                  
              }
        

    

    
    
    }
    
    
    static func registerCards (holderName : String, cardNumber : String, expiryMont : Int,expiryYear:Int, completion: ((JSON) -> Void)?) {
        
                  

            var url = UrlPool.ADD_PAYMENT
               
        var responseJSON = JSON()

        Alamofire.request(url, method: .post, parameters:["alias":"Ev","card_holder_name":holderName,"card_number":cardNumber,"expiry_month":expiryMont,"expiry_year":expiryYear], encoding: URLEncoding.default, headers:FirstViewController.headers).validate().responseJSON { response in
                          
                          
                          switch response.result {
                          case .success(let value):
                              let json = JSON(value)
                              
                              print("get cards")
                              print(json)
            
                              completion!(json)

                          case .failure(let error):
                           print("card error: \(response.debugDescription)")
                        
                           completion!(JSON(["fail":"okey"]))

                            
                          }
                          
                          
                          
                          
                                            
                      
                  
              }
        

    }


}
