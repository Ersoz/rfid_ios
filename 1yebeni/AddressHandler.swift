//
//  AddressHandler.swift
//  1yebeni
//
//  Created by serdar ersöz on 12/11/19.
//  Copyright © 2019 serdar ersöz. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AddressHandler: NSObject {

    static func getCountries ( completion: ((JSON) -> Void)?) {
        
                  

        var url = UrlPool.GET_COUNTRIES
               
        var responseJSON = JSON()

        Alamofire.request(url, method: .get, parameters:nil, encoding: URLEncoding.default, headers:FirstViewController.headers).validate().responseJSON { response in
                          
                          
                          switch response.result {
                          case .success(let value):
                              let json = JSON(value)
                              
                              print("get countries")
                              print(json)
            
                              completion!(json)

                          case .failure(let error):
                            print("get cards error: \(response.debugDescription)")
                            
                          }
                          
                          
                          
                          
                                            
                      
                  
              }
        

    }
    
    static func getAddress ( completion: ((JSON) -> Void)?)  {
         
                   

         var url = UrlPool.REGISTER_ADDRESS
                
         var responseJSON = JSON()

         Alamofire.request(url, method: .get, parameters:nil, encoding: URLEncoding.default, headers:FirstViewController.headers).validate().responseJSON { response in
                           
                           
                           switch response.result {
                           case .success(let value):
                               let json = JSON(value)
                               
                               print("get address")
                               print(json)
             
                               completion!(json)
                            
                            
                               if json.array?.count ?? 0 > 0 {
                                UserDefaults.standard.set(json[0]["address"].string, forKey: "billingAddress")
                                                        UserDefaults.standard.synchronize()

                               }else {
                                
                                let prefs = UserDefaults.standard
                                prefs.removeObject(forKey:"billingAddress")
                            }
                          
                           case .failure(let error):
                             print("get address error: \(response.debugDescription)")
                             if let data = response.data {
                             
                         
                                 let json = String(data: data, encoding: String.Encoding.utf8)
                                 
                                 let jsonError = JSON(data)
                                                                                             
                                 print("Failure Response: \(jsonError)")

                                 completion!(jsonError)
                                                                                          
                                 }
                             
                           }
                           
                           
                           
                           
                                             
                       
                   
               }
         

     }
    
    static func deleteAddress (id:Int, completion: ((JSON) -> Void)?)  {
         
                   

         var url = UrlPool.REGISTER_ADDRESS + "/" + String (id)
                
         var responseJSON = JSON()

         Alamofire.request(url, method: .delete, parameters:nil, encoding: URLEncoding.default, headers:FirstViewController.headers).validate().responseJSON { response in
                           
                           
                           switch response.result {
                           case .success(let value):
                               let json = JSON(value)
                               
                               print("delete address")
                               print(json)
             
                               completion!(json)
                               getAddress { (JSON) in
                                
                            }
                           case .failure(let error):
                             print("delete address error: \(response.debugDescription)")
                             if let data = response.data {
                             
                         
                                 let json = String(data: data, encoding: String.Encoding.utf8)
                                 
                                 let jsonError = JSON(data)
                                                                                             
                                 print("Failure Response: \(jsonError)")

                                 completion!(jsonError)
                                                                                          
                                 }
                             
                           }
                           
                           
                           
                           
                                             
                       
                   
               }
         

     }

    static func RegisterAddress ( country_id:Int,city_id:Int,district:String,address:String, completion: ((JSON) -> Void)?)  {
        
                  

        var url = UrlPool.REGISTER_ADDRESS
               
        var responseJSON = JSON()

        Alamofire.request(url, method: .post, parameters:["country_id":country_id,"city_id":city_id,"district":district,"address":address], encoding: URLEncoding.default, headers:FirstViewController.headers).validate().responseJSON { response in
                          
                          
                          switch response.result {
                          case .success(let value):
                              let json = JSON(value)
                              
                              print("add address")
                              print(json)
            
                              completion!(json)
                            getAddress { (JSON) in
                                                        
                                                    }
                          case .failure(let error):
                            print("get cards error: \(response.debugDescription)")
                            if let data = response.data {
                            
                        
                                let json = String(data: data, encoding: String.Encoding.utf8)
                                
                                let jsonError = JSON(data)
                                                                                            
                                print("Failure Response: \(jsonError)")

                                completion!(jsonError)
                                                                                         
                                }
                            
                          }
                          
                          
                          
                          
                                            
                      
                  
              }
        

    }
    
    static func getCity ( id:String, completion: ((JSON) -> Void)?) {
        
                  

        var url = UrlPool.GET_CITY + id
               
        var responseJSON = JSON()

        Alamofire.request(url, method: .get, parameters:nil, encoding: URLEncoding.default, headers:FirstViewController.headers).validate().responseJSON { response in
                          
                          
                          switch response.result {
                          case .success(let value):
                              let json = JSON(value)
                              
                              print("get countries")
                              print(json)
            
                              completion!(json)

                          case .failure(let error):
                            print("get cards error: \(response.debugDescription)")
                            
                          }
                          
                          
                          
                          
                                            
                      
                  
              }
        

    }
}
