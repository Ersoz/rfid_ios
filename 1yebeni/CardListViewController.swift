//
//  CardListViewController.swift
//  1yebeni
//
//  Created by serdar ersöz on 12/2/19.
//  Copyright © 2019 serdar ersöz. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class CardListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
  

    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var cardTableView: UITableView!
    var cardList = JSON()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let add = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
        navigationItem.rightBarButtonItems = [add]

             

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        
        PaymentHandler.getCards { (JSON) in
                         
            
            print("get credit")
            print(JSON)
            if JSON.array?.count == 0 {
                self.descLabel.isHidden = false
                self.cardTableView.isHidden = true
            }else {
                self.descLabel.isHidden = true
                             self.cardTableView.isHidden = false
            }
            self.cardList = JSON
            self.cardTableView.reloadData()
                     }
    }
    @objc   func  addTapped() -> Void {
        

        print("click")
        var sb = UIStoryboard(name: "Main", bundle: nil)
        
        var popup = sb.instantiateViewController(withIdentifier: "addCardView") as! PaymentMethodViewController
        self.present(popup, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          
        return cardList.array?.count ?? 0
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          
        let  cell = cardTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! paymentMethodTableViewCell
                       

        cell.cardName.text = cardList[indexPath.row]["alias"].string
        cell.cardNumber.text =  (cardList[indexPath.row]["bin_number"].string ?? "") + "*******"
        
        if let isDefault = cardList[indexPath.row]["is_default"].int {
            if isDefault == 1 {
                cell.statusLabel.text = "Birincil Kart"
            }else {
                cell.statusLabel.text = ""

            }
        }
              return cell
        
        
        
      }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {

       
           return true
       }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteButton = UITableViewRowAction(style: .default, title: "Delete") { (action, indexPath) in
            self.cardTableView.dataSource?.tableView!(self.cardTableView, commit: .delete, forRowAt: indexPath)
            return
            
            
            
        }
        let insertButton = UITableViewRowAction(style: .default, title: "Default Card") { (action, indexPath) in
                self.cardTableView.dataSource?.tableView!(self.cardTableView, commit: .insert, forRowAt: indexPath)
                return
            }
        deleteButton.backgroundColor = UIColor.red
        insertButton.backgroundColor = UIColor.orange

        return [deleteButton,insertButton]
    }

    
    
     func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            //objects.remove(at: indexPath.row)
        
            
            
            PaymentHandler.deleteCard(cardID: self.cardList[indexPath.row]["id"].int!) { (JSON) in
                
                print(JSON)
                self.cardList.arrayObject?.remove(at: indexPath.row)

                self.cardTableView.deleteRows(at: [indexPath], with: .fade)

            }
        

        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
            
           
            
            PaymentHandler.setDefault(cardID: self.cardList[indexPath.row]["id"].int!) { (JSON) in
                print("default handler")
                
                self.cardList = JSON
                self.cardTableView.reloadData()
                print(JSON)
            }
        }
    }
      
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
