//
//  LoginViewController.swift
//  1yebeni
//
//  Created by serdar ersöz on 11/29/19.
//  Copyright © 2019 serdar ersöz. All rights reserved.
//

import UIKit
import  Alamofire
import SwiftyJSON
import FirebaseAuth
import PhoneNumberKit
import FlagPhoneNumber

class LoginViewController: UIViewController,FPNTextFieldDelegate {
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name)
    }
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        
        print(isValid)
           print(textField.getFormattedPhoneNumber(format: FPNFormat.E164))
           
           
    
           phoneNumber = textField.getFormattedPhoneNumber(format: FPNFormat.E164) ?? ""
           
        
    }
    
    func fpnDisplayCountryList() {
        
    }

    @IBOutlet weak var fullName: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var emailInput: UITextField!
    
    @IBOutlet weak var loginBtn: UIButton!
    
    @IBOutlet weak var phoneInput: FPNTextField!
    @IBOutlet weak var resendBtn: UIButton!
    var phoneNumber = String()
    @IBOutlet weak var passInput: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardWhenTappedAround()

        phoneInput.delegate = self
        //Auth.auth().settings!.isAppVerificationDisabledForTesting = true

        // Do any additional setup after loading the view.
        self.transNavigationBar()
     
        let backgroundImage = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        backgroundImage.contentMode = UIView.ContentMode.scaleAspectFill
        backgroundImage.image = UIImage(named: "bg_login-1")
                 self.view.insertSubview(backgroundImage, at: 0)
        
        
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent //.default for black style
    }
    @IBAction func loginBtnHandler(_ sender: Any) {
        
        print("phoneefsdfs")
        
        print(phoneNumber)

        print(loginBtn.tag)
        if loginBtn.tag == 0 {
            PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
                       print("verfys")
                       self.view.activityStopAnimating()
                       if let error = error {
                       
                        print("fail")
                        print(error)
                           self.alert(message: error.localizedDescription)
                           return
                       }
                       UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                      print("authh")
                   self.titleLabel.text = "Lütfen telefonunuza gelen onay kodunu giriniz."
                   self.passInput.isHidden = false
                self.loginBtn.tag = 1
                self.loginBtn.setTitle("Onayla", for: .normal)
                self.resendBtn.isHidden = false
            }
        

            

        }    else if loginBtn.tag == 1 {
         
            
            if isValidEmail(emailStr: emailInput.text ?? "") {
                self.sendPassToFirebase()

            }else {
                
                self.alert(message: "Lütfen email adresinizi kontrol ediniz.")
            }
                
        }
        
        
      //  Login()
    }
    func sendPassToFirebase()-> Void {
        
      
        print("verfy")

        
                self.view.activityStartAnimating(activityColor: UIColor.white, backgroundColor: UIColor.black.withAlphaComponent(0.3))
        
        
        
        if let verficationID = UserDefaults.standard.string(forKey: "authVerificationID"){
            
            print(verficationID)
            
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID:verficationID,
            verificationCode: self.passInput.text!)
        
        
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            
            if let error = error {
                // ...
                print(error)
                self.view.activityStopAnimating()
                
        

                self.alert(message: "Lütfen girdiğiniz şifreyi kontrol ediniz.")
                return
            }
            
            // User is signed in
            // ...
           print(Auth.auth().currentUser?.uid)
        
            
            let currentUser = Auth.auth().currentUser
            currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
              if let error = error {
                
                self.alert(message: error.localizedDescription)
                return;
              }

              // Send token to your backend via HTTPS
                print(idToken)
                
                
                self.FirebaseLogin(idToken: idToken!)
            }



            if let displayName = authResult?.user.displayName {
                
                
                //self.FirebaseLogin()
                return
            }else{
                //register
                   //  self.performSegue(withIdentifier: "goToInfo", sender: self)
            }
            
         
            
        }
        }
    }
     func isValidEmail(emailStr:String) -> Bool {

        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    
    
    
    
    @IBAction func resendPassword(_ sender: Any) {
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
                        
                        self.view.activityStopAnimating()
                        if let error = error {
                        
                            self.alert(message: error.localizedDescription)
                            return
                        }
                        UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                       
             }
    }
    func FirebaseLogin(idToken:String)->Void {
           
    
               print("send  id token")
        
    


       
               var url = UrlPool.REGISTER
        
        print(url)
        print("id token \(idToken) name \(self.fullName!) email \(self.emailInput.text!)")
        Alamofire.request(url, method: .post, parameters:["id_token":idToken,"name":self.fullName.text!,"email":self.emailInput.text!,"login_type":1,"phone":self.phoneNumber], encoding: URLEncoding.default, headers:["Accept": "application/json"]).validate().responseJSON { response in
                   
            
            self.view.activityStopAnimating()
            
                   switch response.result {
                   case .success(let value):
                       let json = JSON(value)
                       
                       print("register")
                       print(json)
                       if   let accessToken = json["auth"]["access_token"].string {
                                            
                                      
                                            
                                            UserDefaults.standard.set(accessToken, forKey: "accessToken")
                                            UserDefaults.standard.synchronize()
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                                   
                                                   DispatchQueue.main.async(execute: {
                                                       // work Needs to be done
                                                       
                                                       
                                                       let defaults = UserDefaults.standard
                                                       
                                                       
                                                       
                                                       guard let window = UIApplication.shared.keyWindow else {
                                                           return
                                                       }
                                                       
                                                       print("test2")
                                                       
                                                       guard let rootViewController = window.rootViewController else {
                                                           return
                                                       }
                                                       
                                                       print("test3")
                                                       
                                                       let vc = storyboard.instantiateViewController(withIdentifier: "Main")
                                                       vc.view.frame = rootViewController.view.frame
                                                       vc.view.layoutIfNeeded()
                                                       
                                                       UIView.transition(with: window, duration: 0.9, options: .transitionCrossDissolve, animations: {
                                                           window.rootViewController = vc
                                                       }, completion: { completed in
                                                           // maybe do something here
                                                       })
                                                       
                                                       
                                                       
                                                       
                                                       
                                                       
                                                       
                                                   })
                                                   
                                           
                                            
                       }
                           
                       
              
                       
                       
                   case .failure(let error):
                    
                    if let data = response.data {
                        let json = String(data: data, encoding: String.Encoding.utf8)
                        let jsonError = JSON(data)
                        
                        print("Failure Response: \(jsonError)")
                        self.alert(message: jsonError["messages"][0][0].string ?? jsonError["message"].string ?? "Beklenmedik bir hata oluştu.")
                    }
                   }
                   
                   
                   
                   
                   
               }
               
               
           
       }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
