//
//  AddressesViewController.swift
//  1yebeni
//
//  Created by serdar ersöz on 12/13/19.
//  Copyright © 2019 serdar ersöz. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
class AddressesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {


    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var addressTableView: UITableView!
    var addressArray = JSON()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(
                      self,
                      selector: #selector(addressRefresh),
                      name:NSNotification.Name( "addressComplate"),
                      object: nil)

        
        
        AddressHandler.getAddress(completion: { (JSON) in
                print("address")
                print(JSON)
            self.addressArray  = JSON
           
            if self.addressArray.count == 0 {
                self.descLabel.isHidden = false
                self.addressTableView.isHidden = true
            }else {
                self.descLabel.isHidden = true
                              self.addressTableView.isHidden = false
            }
            self.addressTableView.reloadData()
            })
        
        let add = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
        navigationItem.rightBarButtonItems = [add]


        // Do any additional setup after loading the view.
    }
    
    @objc  func addressRefresh()->Void{
          
          print("refresh")
     
        AddressHandler.getAddress(completion: { (JSON) in
                      print("address")
                      print(JSON)
                  self.addressArray  = JSON
                 
                  if self.addressArray.count == 0 {
                      self.descLabel.isHidden = false
                      self.addressTableView.isHidden = true
                  }else {
                      self.descLabel.isHidden = true
                                    self.addressTableView.isHidden = false
                  }
                  self.addressTableView.reloadData()
                  })
              
                 
      }
    
    
    @objc   func  addTapped() -> Void {
        

        print("click")
        var sb = UIStoryboard(name: "Main", bundle: nil)
        
        var popup = sb.instantiateViewController(withIdentifier: "addAdddressView") as! addressInfoViewController
        self.present(popup, animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return addressArray.array?.count ??  0

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  cell = addressTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! addressCellTableViewCell
        
        cell.addressTitle.text =  self.addressArray[indexPath.row]["city"]["name"].string
                            cell.addressDesc.text =  self.addressArray[indexPath.row]["address"].string

      
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteButton = UITableViewRowAction(style: .default, title: "Delete") { (action, indexPath) in
            self.addressTableView.dataSource?.tableView!(self.addressTableView, commit: .delete, forRowAt: indexPath)
            return
            

            
        }
        return [deleteButton]

    }

    
     func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            //objects.remove(at: indexPath.row)
        
            AddressHandler.deleteAddress(id: addressArray[indexPath.row]["id"].int ?? -1) { (JSON) in

                self.addressArray.arrayObject?.remove(at: indexPath.row)
                self.addressTableView.deleteRows(at: [indexPath], with: .fade)

                    
                  }
            
         
        

        }
    }
      

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
